---
layout: page
title: Research
permalink: /research/
---

- _Deriving Bounds And Inequality Constraints Using Logical Relations Among Counterfactuals_.  
  Noam Finkelstein and Ilya Shpitser, 
  In Proceedings of the Thirty Sixth Conference on Uncertainty in Artificial Intelligence (UAI-20), AUAI Press, 2020.
  [[pdf](/pdfs/uai2020.pdf)][[bibtex](/bibtex/uai2020.txt)][[code](/partial-id)]
