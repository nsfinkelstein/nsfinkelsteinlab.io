---
layout: page
title: Partial Identification
permalink: /partial-id/
---

An implementation of the methods developed in 
_Deriving Bounds And Inequality Constraints Using Logical Relations Among Counterfactuals_
can be found [here](https://www.gitlab.com/nsfinkelstein/partialid).

